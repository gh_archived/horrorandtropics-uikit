const config = {
  testMatch: [`${__dirname}/**/__tests__/**/*.(js|tsx|ts)`],
  "testPathIgnorePatterns": [
    "/node_modules/",
    "/dist/"
  ],
  transform: {
    '^.+\\.tsx?$': 'ts-jest/preprocessor',
    '^.+\\.js$': 'babel-jest',
  },
  globals: {
    'ts-jest': {
      tsConfigFile: './tsconfig.json',
      skipBabel: true
    }
  },
  moduleFileExtensions: ['js', 'ts', 'tsx'],
}

module.exports = config;