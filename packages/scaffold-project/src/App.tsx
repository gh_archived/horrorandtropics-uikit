import { h, Component } from 'preact';

const name = 'hello'

export class App extends Component {
  render() {
    return <h1>props: {name} state: {name}</h1>;
  }
}
