#!/usr/bin/env node

var version = require('../package.json').version
var program = require('commander');

var path = require('path')
var fs = require('fs')

var express = require('express')
var compression = require('compression')
var cors = require('cors')
var helmet = require('helmet')

var http = require('http')
var cluster = require('cluster')

program
  .version(version)
  .option('-s, --settings <path>', 'Settings file')
  .parse(process.argv);

const settingsFile =  path.join(process.cwd(), program.settings || 'serve.json')
const settings = JSON.parse(fs.readFileSync(settingsFile, 'utf8'))

if(!settings.hasOwnProperty('index')) { throw 'settings file is missing index' }
if(!settings.hasOwnProperty('assets')) { throw 'settings file is missing assets' }
if(!settings.hasOwnProperty('csp')) { throw 'settings file is missing csp rules' }

const index = path.join(process.cwd(), settings.index)
const assets = path.join(process.cwd(), settings.assets)

let app = express()
app.use(helmet())
app.use(helmet.referrerPolicy({ policy: 'same-origin' }))
app.use(helmet.contentSecurityPolicy(settings.csp))
app.use(cors())
app.use(compression({level: 9}))
app.use('/', express.static(assets))
app.use('/*', express.static(index))


// Cluseter the node app across all processes

if (cluster.isMaster) {
  // Count the machine's CPUs
  var cpuCount = require('os').cpus().length

  // // Create a worker for each CPU
  for (var i = 0; i < cpuCount; i += 1) {
    cluster.fork()
  }
} else {
  /**
   * Get port from environment and store in Express.
   */

  var port = normalizePort(process.env.PORT || settings.port || '8080')
  app.set('port', port)

  /**
   * Create HTTP server.
   */

  var server = http.createServer(app)

  /**
   * Listen on provided port, on all network interfaces.
   */

  server.listen(port)
  server.on('error', onError)
  server.on('listening', onListening)
}

var restartCount = 0
// Listen for dying workers
cluster.on('exit', function(worker) {
  // Replace the dead worker,
  // we're not sentimental
  restartCount = restartCount + 1
  if (restartCount < 500) {
    cluster.fork()
  }
})

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  var portNorm = parseInt(val, 10)

  if (isNaN(portNorm)) {
    // named pipe
    return val
  }

  if (portNorm >= 0) {
    // port number
    return portNorm
  }

  return false
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error
  }

  var bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      process.exit(1)
    case 'EADDRINUSE':
      process.exit(1)
    default:
      throw error
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  var addr = server.address()
  var bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port
}
