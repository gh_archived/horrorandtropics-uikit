import { h } from 'preact'
import { shallow } from 'preact-render-spy'

import ScaffoldComponent from '../src/index'

test('ScaffoldComponent says hello to props.name', () => {
  // Component context
  const context = shallow(<ScaffoldComponent name='World' />)

  // Component tests
  expect(context.find('p').contains('Hello World!')).toBeTruthy()
})
