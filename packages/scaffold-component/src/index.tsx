import { Component, h } from 'preact'

export interface ScaffoldComponentProps {
  name: string
}

export default class ScaffoldComponent extends Component<ScaffoldComponentProps, any> {
  public render (props) {
    return <p>Hello {props.name}!</p>
  }
}
